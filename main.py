from app import app
import flask
import flask_debugtoolbar
from flask_debugtoolbar_lineprofilerpanel.profile import line_profile


with app.app_context():
    # all your bluprints gets bolted on here
    from booking.views import booking_bp
    app.register_blueprint(booking_bp)

from auth import *
from admin import admin


admin.setup()

# @app.route('/aaa')
# @line_profile
# def hello():
#     result = "<html><body>Checking ...."
#     result+="</body></html>"
#     return result

DEBUG = True

if __name__ == "__main__":
    if DEBUG == True:
        app.debug = True
        app.config['DEBUG_TB_PROFILER_ENABLED'] = True
        app.config['DEBUG_TB_INTERCEPT_REDIRECTS'] = False
        # Specify the debug panels you want
        app.config['DEBUG_TB_PANELS'] = [
            'flask_debugtoolbar.panels.versions.VersionDebugPanel',
            'flask_debugtoolbar.panels.timer.TimerDebugPanel',
            'flask_debugtoolbar.panels.headers.HeaderDebugPanel',
            'flask_debugtoolbar.panels.request_vars.RequestVarsDebugPanel',
            'flask_debugtoolbar.panels.template.TemplateDebugPanel',
            'flask_debugtoolbar.panels.sqlalchemy.SQLAlchemyDebugPanel',
            'flask_debugtoolbar.panels.logger.LoggingPanel',
            'flask_debugtoolbar.panels.profiler.ProfilerDebugPanel',
            # Add the line profiling
            'flask_debugtoolbar_lineprofilerpanel.panels.LineProfilerPanel'
        ]
        toolbar = flask_debugtoolbar.DebugToolbarExtension(app)

    auth.User.create_table(fail_silently=True)

    ## Notes:

    ## This is served as a single process - mutlithreaded webserver
    ## When each request that comes in is run ona separate thread
    ## under this single process.

    ## When the webserver fires up we create the data which can be
    ## accessed/modified from the individual request threads after
    ## acquiring an Event level lock

    ## Data Model -
    ##  Event -> list of rows
    ##  Row -> list of seats
    ##  Seat -> seat contains the status [Available/Unavailable]

    ## Event class has a Lock which is acquired by the the requests
    ## Before performing any one of the tasks
    ##        - booking selected seats
    ##        - booking best seats
    ##        - booking best seats with splits
    ##
    ## Essentially this is a coarse grained lock on the Event object
    ## In a "real system" such a system would you a distributed lock
    ## and possibly an in-memory database/datastore if available or a
    ## very good RDBMS such as Oracle.

    ## Another approach would be to let the database handle the locking
    ## mechanism.
    ## Basically let the threads read concurently from the database
    ## Do the processing..
    ## Try to update the seats with a "database row" level lock.
    ## We are guaranteed only one will be successful.
    ## Handle the failure gracefully.

    app.run(threaded=True)