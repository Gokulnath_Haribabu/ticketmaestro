# Ticketmaestro App

Setup and Run
---------------

```

cd path/to/ticketmaestro

pip install -r dev_requirements.txt

./runserver.sh

```

The booking package has tests that cover the models, processor
and the utils modules

```
# Run Tests
nosetests -v --with-coverage --cover-package=booking

```

Summary:
-----

The ticketmaetro app is a python flask webapp.
I have structured in such a way that the booking functionality
is provided as a 'flask blueprint' which is a nice way to componentise
a flask webapp.

This is served as a single process - mutlithreaded webserver
When each request that comes in is run ona separate thread
under this single process.

When the webserver fires up we create the data which can be
accessed/modified from the individual request threads after
acquiring an Event level lock

Data Model:
- Event -> list of rows
- Row -> list of seats + list contiguous blocks of seats
- Seat -> seat contains the status [Available/Unavailable]

Event class has a Lock which is acquired by the the requests
Before performing any one of the tasks
- booking selected seats
- booking best seats
- booking best seats with splits

Essentially this is a coarse grained lock on the Event object
In a "real system" such a system would you a distributed lock
and possibly an in-memory database/datastore if available or a
very good RDBMS such as Oracle.

Another approach would be to let the database handle the locking
mechanism.
Basically let the threads read concurently from the database
Do the processing..
Try to update the seats with a "database row" level lock.
We are guaranteed only one will be successful.
Handle the failure gracefully.

Assumptions:
-----------

- This server is to be served as a single process multithreaded
  server. The reason is the Data Model does not have any persistence
  functionality. In a real world system this will be provided so you
  will be able to serve up multiple instances and use a distributed lock
  to access the event data.

- Currently the access to the event and the booking does not accomodate the user information.

- Basic form validation is provided and a simplied ui presented.

- Intially tried to go full monty and provide a DB/ORM layer for persistence and then decided against it in favour of performing the
operations (locking, booking.. etc) against in memory python data objects. In view that later on I can potentially add a data/object persistence layer. Basically ignore admin/auth and booking/db_models.

Note:
-----

- I have not provided an implementation of the requirement 4.
  However I have added some thoughts on how to implement it in **BookingProcessor.get_best_available_with_split_optimal** method.




