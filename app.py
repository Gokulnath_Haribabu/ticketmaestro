from flask import Flask

# flask-peewee bindings
from flask_peewee.db import Database

app = Flask(__name__)
#app.config.from_object(__name__)

app.config['SECRET_KEY'] = 'jdsfl3cvmnslvlasdjoewr393ejl3j'

# configure our database
app.config['DATABASE'] = {
        'name': 'example.db',
        'engine': 'peewee.SqliteDatabase',
        'threadlocals': True,
    }

# instantiate the db wrapper
db = Database(app)
