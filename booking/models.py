from threading import Lock

class SeatStatus(object):
    """
        Poor man's enum to represent seat statuses
    """
    AVAILABLE, UNAVAILABLE = range(2)


class Seat(object):

    __slots__ = ['row_number', 'seat_number', 'status']

    def __init__(self, row_number, seat_number):
        self.row_number = row_number
        self.seat_number = seat_number
        self.status = SeatStatus.AVAILABLE

    def get_info(self):
        """
            Provides the seat information to be used for
            displaying to the user.

            returns: tuple(string: key, string: info)
        """
        return ("%s_%s" % (self.row_number, self.seat_number),
                "Row: %s Seat: %s" % (self.row_number, self.seat_number))

    def is_available(self):
        """
            Predicate
        """
        return self.status == SeatStatus.AVAILABLE

    def book(self):
        """
            Books the seat by updating the status and
            possibly other fields such as user info, timestamp etc.
        """
        self.status = SeatStatus.UNAVAILABLE
        # add user info.. timestamp etc

    def unbook(self):
        """
            Un-Books the seat by updating the status and
            possibly other fields such as user info, timestamp etc.
        """
        self.status = SeatStatus.AVAILABLE
        # add user info.. timestamp etc

    def __repr__(self):
        return str({'row_number': self.row_number,
                    'seat_number': self.seat_number,
                    'status': self.status})


class Row(object):
    """
        Each Row object has
        -> a list of seats
        -> a list of contiguous seat blocks

           and provides operations for providing
           a way to quickly allocate / release contiguous
           seat blocks.
    """
    def __init__(self, row_number, no_of_seats=10):
        self.row_number = row_number
        self.no_of_seats = no_of_seats
        # reference to seats list
        self.seats = [ Seat(row_number, i) for i in range(no_of_seats)]
        # also maintain a list of contiguous block(s) of seats
        # in the begining there is only one block
        self.contiguous_seats = [self.seats]

    def get_contiguous_seats(self):
        """
            Generator which yields the next block of
            contiguous seats.

            Yields: List of seats
        """
        for block in self.contiguous_seats:
            yield block

    def allocate(self, block_no, no_of_seats):
        """
            This method is called after a successful
            booking of contiguous seats in a block.
            Because we use a logic similar to
            'first fit' we can assume that the seats
            are booked from the start of the block.
            So we basically slice out the free block and
            stick it in the end of the list of free contiguous seats.

        """
        c_seats = self.contiguous_seats
        c_block = c_seats[block_no]
        right = no_of_seats
        rest = c_block[right:]
        # This operation is O(n-m). Its a tradeoff i am taking
        # for fast lookups by using a list
        del c_seats[block_no]
        if rest:
            c_seats.append(rest)
        if not c_seats:
            self.contiguous_seats = [[]]

    def release(self, seats):
        """
            This method is called after a un-successful
            complete booking of split blocks of contiguous seats.
            At this stage the seats have been unbooked.
            Again, because we use a logic similar to
            'first fit' we can assume that the seats
            are booked from the start of the block.
            So we basically merge back the seats into the free block

        """
        c_seats = self.contiguous_seats
        for i, c_block in enumerate(c_seats):
            if c_block:
                first = c_block[0]
                if (seats[-1].seat_number + 1) == first.seat_number:
                    seats.extend(c_block)
                    c_seats[i] = seats
                    return True
        # edge case
        if len(c_seats) == 1:
            if c_seats[0]:
                c_seats.append(seats)
            else:
                empty = c_seats[0]
                empty.extend(seats)
                c_seats[i] = empty
        else:
            c_seats.append(seats)

    def allocate_seat(self, seat):
        """
            This method is called after a successful booking
            of a seat manually by the user. The alogrithm is a bit clever.
            To find the seat quickly i go to each block and check if the
            seat number falls between the first and the last seat in that block.
            Then I get the index by subtracting the first seat number from the
            seat number of the seat provided as input.
            Once i get the index I slice up the left and the right blocks and
            append them to the end.
        """
        c_seats = self.contiguous_seats

        for block_no, c_block in enumerate(c_seats):
            if c_block:
                first, last = c_block[0], c_block[-1]
                if (seat.seat_number >= first.seat_number and
                    seat.seat_number <=last.seat_number):
                    index = seat.seat_number - first.seat_number
                    pre, post = c_block[:index], c_block[index+1:]
                    del c_seats[block_no]
                    if pre:
                        c_seats.append(pre)
                    if post:
                        c_seats.append(post)
                    if not c_seats:
                        self.contiguous_seats = [[]]
                    break


    def __repr__(self):
        return repr(self.__dict__)


class Event(object):
    """
        Each event is represented as
        -> list of row objects each
    """
    def __init__(self, name, event_id, no_of_rows=10, no_seats_per_row=10):
        self.name = name
        self.event_id = event_id
        self.rows = [Row(i, no_seats_per_row) for i in range(no_of_rows)]
        self.lock = Lock()

    def get_contiguous_seats(self):
        """
            Generator function yielding the next contiguous
            block and addtional information for efficient
            'allocation/deallocation'

            Yields: Tuple( Tuple(<row number>, <block number>),
                            List of seats))
        """
        for row in self.rows:
            for i, block in enumerate(row.get_contiguous_seats()):
                if block:
                    yield ((row.row_number, i), block)

    def regroup_contiguous_seats(self, seats, t):
        """
           Given a contiguous list of seats and the
           tuple containing where to find it,
           'allocate the block' and regroup the cached
           contiguous seats list
        """
        row_number, block_no = t
        row = self.rows[row_number]
        row.allocate(block_no, len(seats))

    def release_contiguous_seats(self, seats, t):
        """
           Given a contiguous list of seats and the
           tuple containing where to find it,
           'release the block' into the available seats pool.
        """
        row_number, block_no = t
        row = self.rows[row_number]
        # we cant reliably use block_no from which the seats were sliced out
        row.release(seats)

    def regroup_seat(self, seat):
        """
           When we allocate the seats then we have control over
           where we are placing the seats.

           But if the user requests a specific seat we need to
           'allocate that seat' and fragment the contiguous block
           where this seat this present.
        """
        row = self.rows[seat.row_number]
        row.allocate_seat(seat)

    def get_seat_choices(self):
        """
           Provides the seat information to be displayed to
           the user.

           Returns: List of seat information

        """
        seats = []
        for row in self.rows:
            for seat in row.seats:
                seats.append(seat.get_info())
        return seats

    def get_seats(self, seat_choices):
        """
            After we get back the request from the user
            we parse the info back into the seat objects.

            Returns: List of seats
        """
        seat_tuples = self._parse_seat_info(seat_choices)
        seats = []
        for seat_tuple in seat_tuples:
            row_number, seat_number = seat_tuple
            seat = self.rows[row_number].seats[seat_number]
            seats.append(seat)
        return seats

    def _parse_seat_info(self, seat_choices):
        """
            Helper function to parse the info.

            Returns: List of Tuple(<row number>, <seat number>)
        """
        seat_tuples = []
        for seat_choice in seat_choices:
            row_number, seat_number = map(int, seat_choice.split('_'))
            seat_tuples.append((row_number, seat_number))
        return seat_tuples


class Events(object):
    """
    Note:
        cache events in memory
        ideally you would be getting this from the
        database/in-memory cache..
        this happens when the webserver fires up
    """

    EVENT_NAMES = [('1', 'The_Lion_King'), ('2', 'Bewitched'), ('3', 'Urinetown')]
    EVENT_NAMES_DICT = dict(EVENT_NAMES)
    events = {name: Event(name, i) for i, name in EVENT_NAMES}

    @classmethod
    def get_event(cls, event_id):
        """
           Provides the event object for the given event_id
        """
        event_name = cls.EVENT_NAMES_DICT.get(event_id)
        event = cls.events.get(event_name)
        return event

    @classmethod
    def get_all_events(cls):
        """
           Provides all the events sorted by their event_id
        """
        events = sorted(cls.events.values(), key=lambda x: x.event_id)
        return events

