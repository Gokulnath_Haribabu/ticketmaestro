from flask_wtf import Form
from wtforms import SelectField, IntegerField, SelectMultipleField
from wtforms import validators


OPTIONS = [('manual', 'Choose by myself'),
           ('autobest', 'Best seats available'),
           ('autobest_with_split', 'Best seats with split'),]



def validate_no_of_seats():
    """
        Custom validator to validate no_of_seats based on
        the OPTIONS provided
    """
    MIN_VALUE = 1
    MIN_VALUE_SPLITS = 4
    MAX_VALUE = 10
    message = 'Must select between %d and %d seats.'

    def _validate_no_of_seats(form, field):
        l = field.data and int(field.data) or 0
        if form['option'].data == 'autobest':
            if l < MIN_VALUE  or l > MAX_VALUE:
                raise validators.ValidationError(message % (MIN_VALUE,
                    MAX_VALUE))
        elif form['option'].data == 'autobest_with_split':
            if l < MIN_VALUE_SPLITS or l > MAX_VALUE:
                raise validators.ValidationError(message % (MIN_VALUE_SPLITS,
                    MAX_VALUE))

    return _validate_no_of_seats

def validate_seats():
    """
        Custom validator to validate seats based on
        the OPTIONS provided
    """
    MIN_SEATS = 1
    MAX_SEATS = 10
    message = 'Must select between %d and %d seats.' % (MIN_SEATS, MAX_SEATS)

    def _validate_seats(form, field):
        l = field.data and len(field.data) or 0
        if form['option'].data == 'manual':
            if l <= 0 or l > MAX_SEATS:
                raise validators.ValidationError(message)

    return _validate_seats


class BookingForm(Form):
    option = SelectField(u'Option', choices=OPTIONS)
    seats = SelectMultipleField(u'Seats',
                    [validate_seats()])
    no_of_seats = IntegerField(u'No of seats',
                    [validate_no_of_seats()], default=0)



