# """

#    This stuff is WIP. The idea was to use a DBMS

#    for persisting and retrieving the Model data.

#    But decided against it to concentrate on providing

#    and in-memory data model respresentation.

# """



# from peewee import *
# from app import db


# class BaseModel(Model):

#     class Meta:
#         database = db


# class Event(BaseModel):

#     name = CharField()

#     def get_contiguous_seats(self):
#         contiguous_seats = []
#         prev_row_number = 0
#         for seat in self.seats:
#             if seat.status == SeatStatus.AVAILABLE and prev_row_number == seat.row_number:
#                 contiguous_seats.append(seat)
#                 prev_row_number = seat.row_number
#             else:
#                 yield contiguous_seats
#                 contiguous_seats = []
#         yield contiguous_seats

#     def get_seat_choices(self):
#         seats = []
#         for seat in self.seats:
#             seats.append(seat.get_info())
#         return seats

#     def get_seats(self, seat_choices):
#         seat_tuples = self._parse_seat_info(seat_choices)
#         seats = []
#         ## not correct ?
#         for seat_tuple in seat_tuples:
#             row_number, seat_number = seat_tuple
#             seat = self.rows[row_number].seats[seat_number]
#             seats.append(seat)
#         return seats

#     def _parse_seat_info(self, seat_choices):
#         seat_tuples = []
#         for seat_choice in seat_choices:
#             row_number, seat_number = map(int, seat_choice.split('_'))
#             seat_tuples.append((row_number, seat_number))
#         return seat_tuples

#     def __unicode__(self):
#         return self.name


# class SeatStatus(object):
#     AVAILABLE, UNAVAILABLE = ('0', '1')


# class Seat(BaseModel):
#     row_number = IntegerField()
#     seat_number = IntegerField()
#     event = ForeignKeyField(related_name='seats')
#     status = CharField(default=SeatStatus.AVAILABLE)

#     def get_info(self):
#         return ("%s_%s" % (self.row_number, self.seat_number),
#                 "Row: %s Seat: %s" % (self.row_number, self.seat_number))

#     def is_available(self):
#         return self.status == SeatStatus.AVAILABLE

#     def book(self):
#         self.status = SeatStatus.UNAVAILABLE
#         # add user info.. timestamp etc

#     def __unicode__(self):
#         return "%s_%s" % (self.row_number, self.seat_number)


# class Events(object):

#     EVENT_NAMES = [('1', 'The_Lion_King'), ('2', 'Bewitched'), ('3', 'Urinetown')]
#     EVENT_NAMES_DICT = dict(EVENT_NAMES)
#     events = {name: Event(name, i) for i, name in EVENT_NAMES}

#     @classmethod
#     def get_event(cls, event_id):
#         event_name = cls.EVENT_NAMES_DICT.get(event_id)
#         event = cls.events.get(event_name)
#         return event

#     @classmethod
#     def get_all_events(cls):
#         events = sorted(cls.events.values(), key=lambda x: x.event_id)
#         return events
