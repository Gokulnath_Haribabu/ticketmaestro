from ..processor import BookingProcessor, BookingError
from ..models import Seat, SeatStatus, Row, Event
import unittest

class TestBookingProccessor(unittest.TestCase):
    def setUp(self):
        no_of_rows = 1
        no_seats_per_row = 10
        self.event = Event('dummy', 1, no_of_rows, no_seats_per_row)

    def test_process_manual(self):
        option = 'manual'
        seats = [(0, 4), ]
        choices = ["%s_%s" % x for x in seats]
        processor = BookingProcessor(self.event, option, seat_choices=choices)
        processor.process()
        for row_number, seat_number in seats:
            self.assertFalse(self.event.rows[row_number].seats[seat_number].is_available())

    def test_process_manual_contention(self):
        option = 'manual'
        seats = [(0, 4), ]
        choices = ["%s_%s" % x for x in seats]
        processor = BookingProcessor(self.event, option, seat_choices=choices)
        processor.process()
        with self.assertRaises(BookingError):
            processor1 = BookingProcessor(self.event, option, seat_choices=choices)
            processor1.process()

    def test_process_autobest(self):
        option = 'autobest'
        no_of_seats = 4
        processor = BookingProcessor(self.event, option, no_of_seats=no_of_seats)
        processor.process()
        for i in range(no_of_seats):
            self.assertFalse(self.event.rows[0].seats[i].is_available())

    def test_process_autobest_not_available(self):
        option = 'autobest'
        no_of_seats = 4
        processor = BookingProcessor(self.event, option, no_of_seats=no_of_seats)
        processor.process()

        processor = BookingProcessor(self.event, option, no_of_seats=no_of_seats)
        processor.process()

        with self.assertRaises(BookingError):
            processor1 = BookingProcessor(self.event, option, no_of_seats=no_of_seats)
            processor1.process()

    def test_process_autobest_with_split_scenario(self):
        option = 'manual'
        seats = [(0, 2), (0,3), (0, 6), (0, 7)]
        choices = ["%s_%s" % x for x in seats]
        processor = BookingProcessor(self.event, option, seat_choices=choices)
        processor.process()

        option = 'autobest_with_split'
        no_of_seats = 7
        with self.assertRaises(BookingError):
            processor1 = BookingProcessor(self.event, option, no_of_seats=no_of_seats)
            processor1.process()

        print self.event.rows[0].contiguous_seats
        no_of_seats = 6
        processor1 = BookingProcessor(self.event, option, no_of_seats=no_of_seats)
        processor1.process()

        for row in self.event.rows:
            for seat in row.seats:
                self.assertFalse(seat.is_available())