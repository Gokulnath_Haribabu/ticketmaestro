from ..models import Seat, SeatStatus, Row, Event
import unittest
import random

class TestSeat(unittest.TestCase):

    def setUp(self):
        row = 5
        seat = 5
        self.seat = Seat(row, seat)

    def test_seat_book(self):
        self.seat.book()
        self.assertEquals(self.seat.status, SeatStatus.UNAVAILABLE)

    def test_seat_unbook(self):
        self.assertEquals(self.seat.status, SeatStatus.AVAILABLE)
        self.seat.book()
        self.seat.unbook()
        self.assertEquals(self.seat.status, SeatStatus.AVAILABLE)

    def test_is_available(self):
        self.assertTrue(self.seat.is_available())
        self.seat.book()
        self.assertFalse(self.seat.is_available())

    def test_seat_info(self):
        self.assertEquals(len(self.seat.get_info()), 2)
        assert isinstance(self.seat.get_info(), tuple), 'Should return a tuple'

class TestRow(unittest.TestCase):

    def setUp(self):
        row_number = 5
        self.no_of_seats = 10
        self.row = Row(row_number, no_of_seats=self.no_of_seats)

    def test_init(self):
        self.assertEquals(len(self.row.contiguous_seats), 1)
        self.assertEquals(len(self.row.seats), self.row.no_of_seats)

    def test_allocate_and_release_random(self):
        block_no = 0
        no_of_seats = random.randint(1, self.row.no_of_seats)
        self.row.allocate(block_no, no_of_seats)
        self.assertEquals(len(self.row.contiguous_seats), 1)
        self.assertEquals(len(self.row.contiguous_seats[block_no]),
            self.row.no_of_seats - no_of_seats)
        self.row.release([Seat(self.row.row_number, i) for i in range(no_of_seats)])
        self.assertEquals(len(self.row.contiguous_seats[block_no]),
            self.no_of_seats)

    def test_allocate_and_release_all(self):
        block_no = 0
        no_of_seats = self.row.no_of_seats
        self.row.allocate(block_no, no_of_seats)
        self.assertEquals(len(self.row.contiguous_seats), 1)
        self.assertEquals(len(self.row.contiguous_seats[block_no]),
            self.row.no_of_seats - no_of_seats)

        self.row.release([Seat(self.row.row_number, i) for i in range(no_of_seats)])

        self.assertEquals(len(self.row.contiguous_seats[block_no]),
            self.row.no_of_seats)

    def test_allocate_single_requested_seat(self):
        no_of_seats = self.row.no_of_seats
        seat_number = 5
        seat = Seat(self.row.row_number, seat_number)
        self.row.allocate_seat(seat)
        self.assertEquals(len(self.row.contiguous_seats), 2) # has split now

        self.assertEquals(len(self.row.contiguous_seats[0]),
            seat_number)

        self.assertEquals(len(self.row.contiguous_seats[1]),
            (self.row.no_of_seats -1) - seat_number)

    def test_allocate_single_requested_seat_left_edge(self):
        no_of_seats = self.row.no_of_seats
        seat_number = self.row.no_of_seats - 1
        seat = Seat(self.row.row_number, seat_number)
        self.row.allocate_seat(seat)
        self.assertEquals(len(self.row.contiguous_seats), 1) # has split now

        self.assertEquals(len(self.row.contiguous_seats[0]),
            self.row.no_of_seats - 1)

    def test_allocate_single_requested_seat_right_edge(self):
        no_of_seats = self.row.no_of_seats
        seat_number = 0
        seat = Seat(self.row.row_number, seat_number)
        self.row.allocate_seat(seat)
        self.assertEquals(len(self.row.contiguous_seats), 1) # has not split

        self.assertEquals(len(self.row.contiguous_seats[0]),
            self.row.no_of_seats - 1)


    def test_edge_case(self):
        seat_number = 5
        seat = Seat(self.row.row_number, seat_number)
        self.row.allocate_seat(seat)
        self.assertEquals(len(self.row.contiguous_seats), 2) # to 2
        block_no = 1
        self.row.allocate(block_no, (self.row.no_of_seats -1) - seat_number)
        self.assertEquals(len(self.row.contiguous_seats), 1) # back to 1
        self.row.release([Seat(self.row.row_number, i) for i in range((self.row.no_of_seats -1) - seat_number)])
        self.assertEquals(len([x for x in self.row.get_contiguous_seats()]), 2) # to 2 again


class TestEvent(unittest.TestCase):

    def setUp(self):
        no_of_rows = 1
        no_seats_per_row = 10
        self.event = Event('dummy', 1, no_of_rows, no_seats_per_row)

    def test_event_scenario_1(self):
        row_number = 0
        seat_number = 5
        seat = Seat(row_number, seat_number)
        self.assertEquals(len([x for x in self.event.get_contiguous_seats()]), 1)
        self.event.regroup_seat(seat)

        self.assertEquals(len([x for x in self.event.get_contiguous_seats()]), 2) # split to 2

        seats = [Seat(row_number, i) for i in range(seat_number)]
        self.event.regroup_contiguous_seats(seats, (row_number, 0))

        self.assertEquals(len([x for x in self.event.get_contiguous_seats()]), 1) # to 1

        self.event.release_contiguous_seats(seats, (row_number, 0))

        self.assertEquals(len([x for x in self.event.get_contiguous_seats()]), 2) # split to 2


