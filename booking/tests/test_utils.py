from ..utils import *

import unittest

class TestUtils(unittest.TestCase):

    def setUp(self):
        self.d = {
                    4: [(4,), (2, 2)],
                    5: [(5,), (3, 2)],
                    6: [(6,), (4, 2), (3, 3), (2, 2, 2)],
                    7: [(7,), (5, 2), (4, 3), (3, 2, 2)],
                    8: [(8,), (6, 2), (5, 3), (4, 4), (4, 2, 2), (3, 3, 2), (2, 2, 2, 2)],
                    9: [(9,), (7, 2), (6, 3), (5, 4), (5, 2, 2), (4, 3, 2), (3, 3, 3), (3, 2, 2, 2)],
                    10: [(10,), (8, 2), (7, 3), (6, 4), (6, 2, 2), (5, 5), (5, 3, 2), (4, 4, 2), (4, 3, 3), (4, 2, 2, 2), (3, 3, 2, 2), (2, 2, 2, 2, 2)],
                  }

    def test_partitions(self):
        for n, expected in self.d.items():
            actual = [x for x in partitionsOf(n)]
            self.assertListEqual(expected, actual)