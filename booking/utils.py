from itertools import tee
from types import GeneratorType

Tee = tee([], 1)[0].__class__

def memoized(f):
    cache={}
    def ret(*args):
        if args not in cache:
            cache[args]=f(*args)
        if isinstance(cache[args], (GeneratorType, Tee)):
            # the original can't be used any more,
            # so we need to change the cache as well
            cache[args], r = tee(cache[args])
            return r
        return cache[args]
    return ret

# reference : http://stackoverflow.com/a/10726355

@memoized
def _partitionsOf1(num,lt):
    if not num:
        yield ()
    for i in range(min(num,lt),1,-1):
        for parts in _partitionsOf1(num-i,i):
            yield (i,)+parts

def partitionsOf(num):
    for part in _partitionsOf1(num,num):
        yield part


# reference : http://code.activestate.com/recipes/218332-generator-for-integer-partitions/#c4


#others:
#http://jeromekelleher.net/partitions.php
#http://code.activestate.com/recipes/218332-generator-for-integer-partitions/