import flask
import time
from datetime import datetime
from .forms import BookingForm
from .models import Events, Event
from .processor import BookingProcessor, BookingError
from pprint import pprint

booking_bp = flask.Blueprint('booking', __name__)



@booking_bp.route('/', methods=['GET'])
def index():
    return flask.render_template('booking_index.html',
        events=Events.get_all_events())


@booking_bp.route('/event/<event_id>', methods=['GET', 'POST'])
def event(event_id):
    form = BookingForm(flask.request.form)
    event = Events.get_event(event_id)
    if not event:
        return flask.redirect('booking.index')
    form.seats.choices = event.get_seat_choices()
    if flask.request.method == 'POST' and form.validate():
        #process
        processor = BookingProcessor(event,
            form.option.data,
            form.seats.data,
            form.no_of_seats.data)
        try:
            processor.process()
        except BookingError, e:
            flask.flash(e.msg, category='danger')
        else:
            flask.flash(u'Booking complete', category='success')
    pprint([x.contiguous_seats for x in event.rows], depth=5)
    return flask.render_template('booking.html', form=form, event=event)
