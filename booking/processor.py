import time
from datetime import datetime
import flask
from pprint import pprint

from .utils import partitionsOf

class Error(Exception):
    pass

class BookingError(Error):
    """
        Any exception while performing the booking operation
    """
    def __init__(self, msg):
        self.msg = msg


class BookingProcessor(object):
    """
        Based on the user input provided, handles the booking process.

        Before starting the booking we try to acquire a blocking lock on
        the event on which we are going to perform the booking.

        When we run our server we process each request in a separate
        thread. So each request (in a thread) gets an exclusive shot at
        the booking process.

        General usage:
            processor = BookingProcessor(event, option, .....)
            processor.process()

        Exception : BookingError

    """
    def __init__(self, event, option,
                        seat_choices=None, no_of_seats=None):
        self.event = event
        self.option = option
        self.seat_choices = seat_choices
        self.no_of_seats = no_of_seats

    def process(self):
        """
            Based on the option provided, the appropriate booking
            process is called.
        """
        if self.option == "manual":
            self.book_seats()
        elif self.option == "autobest":
            self.get_best_available()
        elif self.option == "autobest_with_split":
            self.get_best_available_with_split()

    def book_seats(self):
        """
            Booking process for user selected seat(s)
            Basically requirement 1)
        """
        with self.event.lock:
            seats = self.event.get_seats(self.seat_choices)
            if self.are_seats_available(seats):
                for seat in seats:
                    seat.book()
                    self.event.regroup_seat(seat)
            else:
                raise BookingError("Contention detected..")

    def get_best_available(self):
        """
            Booking process for best available seats
            Basically requirement 2)
        """
        with self.event.lock:
            t, seats = self.get_available_seats(self.no_of_seats)
            if seats:
                self.book_contiguous(seats, t)
                #pprint([x.contiguous_seats for x in self.event.rows])
            else:
                raise BookingError("No seats available for selection")

    def get_best_available_with_split(self):
        """
            Booking process for best available seats with
            splitting option.
            Basically requirement 3)
        """
        with self.event.lock:
            for partition in partitionsOf(self.no_of_seats):
                split_seats = []
                flag = True
                for n in partition:
                    t, seats = self.get_available_seats(n)
                    if not seats:
                        flag = False
                        break
                    self.book_contiguous(seats, t)
                    split_seats.append((t, seats))
                if not flag:
                    for t, seats in split_seats:
                        self.unbook_contiguous(seats, t)
                else:
                    return True
            raise BookingError("No seats available for selection")

    def get_best_available_with_split_optimal(self):
        """
            :TODO
            Do something similiar to what I did with the requirement 3)
            But also rank each successful booking on
               -> row number
               -> no of contiguous seats acquired
            food for thought!
        """
        pass

    def get_available_seats(self, n):
        """
            scan for the next contiguous seat block
            with atleast 'n' elements.

            returns Tuple( Tuple(<row number>, <block number>),
                            List of seats))
        """
        for t, seats in self.event.get_contiguous_seats():
            if n <= len(seats):
                return (t, seats[:n])
        return (None, None)

    def book_contiguous(self, seats, t):
        """
            Given a list of seats and the tuple containing the
            location of the seats ( in the contiguous seat block),
            book the seats and regroup accordingly.

            Exception: BookingError
        """
        if self.are_seats_available(seats):
            for seat in seats:
                seat.book()
            self.event.regroup_contiguous_seats(seats, t)
        else:
            raise BookingError("Contention detected..")

    def unbook_contiguous(self, seats, t):
        """
            Given a list of seats and the tuple containing the
            location of the seats ( in the contiguous seat block),
            un-book the seats and release accordingly.

            Exception: BookingError
        """
        for seat in seats:
            seat.unbook()
        self.event.release_contiguous_seats(seats, t)

    def are_seats_available(self, seats):
        """
            Predicate. Checks if all seats are available to book.
        """
        is_available = reduce(lambda x, y: x and y,
                              map(lambda seat: seat.is_available(),
                                   seats))
        return is_available
